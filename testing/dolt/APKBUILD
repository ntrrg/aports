# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=dolt
pkgver=0.40.26
pkgrel=0
pkgdesc="Dolt – It's Git for Data"
url="https://www.dolthub.com"
arch="all !x86 !armhf !armv7" # fails on 32-bit
license="Apache-2.0"
options="!check chmod-clean"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/dolthub/dolt/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver/go"

export GOCACHE="$srcdir/go-cache"
export GOTMPDIR="$srcdir"
export GOMODCACHE="$srcdir/go"

build() {
	mkdir -p build
	go build \
		-trimpath \
		-buildmode=pie \
		-mod=readonly \
		-modcacherw \
		-ldflags "-extldflags=\"$LDFLAGS\"" \
		-o build \
		./cmd/...
}

package() {
	install -Dm755 build/dolt "$pkgdir"/usr/bin/dolt
	install -Dm755 build/dolt "$pkgdir"/usr/bin/git-dolt
	install -Dm755 build/dolt "$pkgdir"/usr/bin/git-dolt-smudge
}

sha512sums="
352a893c3a6ca9240c79ff579bb1b89fec06ee6da6e824c09689c8bf76b5df7df3dd0c1ea3c3cb760812b9ec83c8effab7495aa4c28efcf4f1f936bb163967f4  dolt-0.40.26.tar.gz
"
